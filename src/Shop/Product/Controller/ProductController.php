<?php
namespace Shop\Product\Controller;

use Silex\Application;


class ProductController
{
    public function index()
    {
        return 'Products list';
    }

    public function edit(Application $app, $id)
    {
        return $app['twig']->render('form.twig', array(
            'id' => $id,
        ));
    }

    public function show(Application $app, $id)
    {
        $sql = "SELECT * FROM products";
        $products = $app['db']->fetchAssoc($sql, array((int) $id));

        return $app['twig']->render('product.twig', array(
            'id' => $id,
            'products'=>$products
        ));
    }

    public function store()
    {
        return 'Create new product';
    }

    public function update($id)
    {
        return 'Update product ' . $id;
    }

    public function destroy($id)
    {
        return 'Delete product ' . $id;
    }

}