<?php
namespace Shop\Product\Service;

use Silex\Application;
use Silex\ControllerProviderInterface;

class ProductControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $products = $app['controllers_factory'];

        $products->get("/", "Shop\\Product\\Controller\\ProductController::index");

        $products->post("/", "Shop\\Product\\Controller\\ProductController::store");

        $products->get("/{id}", "Shop\\Product\\Controller\\ProductController::show");

        $products->get("/edit/{id}", "Shop\\Product\\Controller\\ProductController::edit");

        $products->put("/{id}", "Shop\\Product\\Controller\\ProductController::update");

        $products->delete("/{id}", "Shop\\Product\\Controller\\ProductController::destroy");

        return $products;
    }
}