<?php

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../resources/views',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), $app['config']['db.options']);