<?php

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

/**
 *
 */
$app['config'] = require_once __DIR__ . '/config/config.php';

/**
 *
 */
require_once 'providers.php';

/**
 *
 */
require_once 'routes.php';

/**
 *
 */
$app['debug'] = $app['config']['app']['debug'];
