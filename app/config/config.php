<?php
$config = [

    "app" => [
        "debug" => true,
    ],

    'db.options' => [
        'mysql' => [
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'db',
            'user' => 'root',
            'password' => 'root',
            'charset' => 'utf8mb4',
        ],
    ],
];

return $config;